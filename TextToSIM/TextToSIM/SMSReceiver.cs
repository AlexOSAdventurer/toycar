﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Telephony;
using Android.Util;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Database;
using Java.Interop;

namespace TextToSIM
{
    [BroadcastReceiver(Enabled = true, Exported = true)]
    class SMSReceiver : BroadcastReceiver
    {

        public SMSReceiver()
        {

        }
   
        public override void OnReceive(Context context, Intent intent)
        {
            Log.Debug("SMSReceiver", "Got something!");
            SmsMessage[] messages = Android.Provider.Telephony.Sms.Intents.GetMessagesFromIntent(intent);
            string path = "/storage/emulated/0/Download/sms";
            StreamWriter f = new StreamWriter(path);
            f.WriteLine(messages[0].DisplayMessageBody);
            f.Close();
            Log.Debug("SMSReceiver", messages[0].DisplayMessageBody);
            Log.Debug("SMSReceiver", path);
        }
    }

}