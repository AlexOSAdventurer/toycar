﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Telephony;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TextToSIM
{
    [Service]
    public class SIMService : Service
    {
        SMSReceiver receiver = null;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }
        public override void OnCreate()
        {
            try
            {
                IntentFilter filter = new IntentFilter(Android.Provider.Telephony.Sms.Intents.SmsReceivedAction);
                SMSReceiver receiver_temp = new SMSReceiver();
                RegisterReceiver(receiver_temp, filter);
                receiver = receiver_temp;
                Log.Debug("TextToSIM Service", "Service setup done.");
            }
            catch (Exception p)
            {
                Log.Debug("TextToSIM Service", p.Message);
            }
            
        }
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Log.Debug("TextToSIM Service", "Received start command.");
            return StartCommandResult.Sticky;
        }
        public override void OnDestroy()
        {
            UnregisterReceiver(receiver);
            base.OnDestroy();
        }
    }
}