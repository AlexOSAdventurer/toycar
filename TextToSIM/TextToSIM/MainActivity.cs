﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Util;
using System;
using Android.Telephony;

namespace TextToSIM
{
    [Activity(Label = "TextToSIM", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {

        Switch toggle;

        Intent serv;

        bool Status = false;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);

            toggle = FindViewById<Switch>(Resource.Id.Toggle);
            toggle.Click += ToggleHandler;
        }

        void ToggleHandler(object sender, EventArgs e)
        {
            Status = toggle.Checked;
            if (Status)
            {
                serv = new Intent(this,  typeof(SIMService));
                Log.Debug("TextToSIM", "Service starting.");
                StartService(serv);
            }
            else
            {
                Log.Debug("TextToSIM", "Service stopping.");
                StopService(serv);
            }
        }
        
    }
}

