import RPi.GPIO as io

class Motor(object):
	
	def __init__(self, port1, port2, freq):
		self.p1 = port1
		self.p2 = port2
		self.hz = freq
		self.SetIO()
	
	def __del__(self):
		self.PWM1.stop()
		self.PWM2.stop()

	def SetIO(self):
		io.setwarnings(False)
		io.setmode(io.BCM)
		io.setup(self.p1, io.OUT)
		io.setup(self.p2, io.OUT)
		self.PWM1 = io.PWM(self.p1, self.hz)
		self.PWM2 = io.PWM(self.p2, self.hz)
		self.PWM1.start(0)
		self.PWM2.start(0)

	def SetSpeed(self, speed):
		print(speed)
		if speed > 0:
			self.PWM2.ChangeDutyCycle(0)
			self.PWM1.ChangeDutyCycle(speed)
		else:
			self.PWM1.ChangeDutyCycle(0)
			self.PWM2.ChangeDutyCycle(-speed)
	
