import RPi.GPIO as io
import time
from os import system
from motor_class import Motor


class App(object):

	M1 = None
	M2 = None
	def drive(self, x, z):
		self.M1.SetSpeed(x)
		self.M2.SetSpeed(z)

	def cexit(self):
   		del self.M1
   		del self.M2
		io.cleanup()
    		exit()

	def poll(self):
		system("adb pull /storage/emulated/0/Download/sms")

	def read(self):
		self.poll()
		f = open("sms", "r")
		s = f.read()
		numbers = []
		for t in s.split():
			try:
				numbers.append(float(t))
			except ValueError:
				pass
		if (len(numbers) == 0):
			numbers = [0, 0]
		return numbers

	def __init__ (self):
		self.M1 = Motor(4, 17, 500)
		self.M2 = Motor(25, 22, 500)
		while True:
	   		numbers = self.read()
			self.drive(numbers[0], numbers[1])
			print(str(numbers[0]) + " " + str(numbers[1]))
			time.sleep(0.5)

nApp = App()
